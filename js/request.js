var styleElems = document.querySelectorAll('#styles input[type=radio]');
var tipElems = document.querySelectorAll('#tips input[type=radio]');
var totalElem = document.querySelector('#total span');
var radioElems = document.querySelectorAll('input[type=radio]');
var totalPrice = 0;

//add event listener to all radio buttons
radioElems.forEach(function(radioElem) {
  radioElem.addEventListener('click', changeTotal);
});

function changeTotal() {
  totalPrice = 0;

  //add style price
  styleElems.forEach(function(styleElem) {
    if (styleElem.checked) {
      totalPrice += Number(styleElem.value);
    }
  });

  //add tip
  tipElems.forEach(function(tipElem) {
    if (tipElem.checked) {
      totalPrice += totalPrice * Number(tipElem.value);
    }
  });

  totalElem.innerHTML = totalPrice.toFixed(2); //nearest hundredth
}